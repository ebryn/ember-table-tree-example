import Ember from 'ember';
import createData from '../utils/create-data';

var get = Ember.get;

// NOTE: This is code you shouldn't copy, it's purely for fake data generation
export default Ember.Service.extend({
  controller: Ember.computed(function() {
    // mega hack, don't do this at home kids
    return this.container.lookup('controller:index');
  }),
  columns: Ember.computed.oneWay('controller.model.columns'),

  fetch(row) {
    let columns = this.get('columns');
    return new Ember.RSVP.Promise(resolve => {
      Ember.run.later(() => {
        resolve(createData(columns, 100, get(row, 'id')));
      }, 2000);
    });
  }
});
