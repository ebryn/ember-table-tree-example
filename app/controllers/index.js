import Ember from 'ember';
import Column from 'ember-table/models/column-definition';

var get = Ember.get;

export default Ember.Controller.extend({
  columns: Ember.computed.map('model.columns', function(column){
    var attrs = {};
    if (get(column, 'headerCellName') === 'Name') {
      attrs = {
        tableCellView: 'tree-table/collapsible-table-cell',
        textAlign: 'text-align-left'
      };
    }
    var attributes = Ember.merge(column, attrs);
    return Column.create(attributes);
  }),
});
